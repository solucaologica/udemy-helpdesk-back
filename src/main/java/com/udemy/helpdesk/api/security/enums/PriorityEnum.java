package com.udemy.helpdesk.api.security.enums;

public enum PriorityEnum {
	High,
	Normal,
	Low
}