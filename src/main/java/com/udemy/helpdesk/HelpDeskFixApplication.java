package com.udemy.helpdesk;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;


import com.udemy.helpdesk.api.repository.UserRepository;
import com.udemy.helpdesk.api.security.entity.User;
import com.udemy.helpdesk.api.security.enums.ProfileEnum;

@SpringBootApplication
public class HelpDeskFixApplication {
	 /**
	  * @author 
	  * @param args
	  */
	public static void main(String[] args) {
		SpringApplication.run(HelpDeskFixApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		return args -> {
			initUsers(userRepository, passwordEncoder);
		}; 
	}

	private void initUsers(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		User admin = new User();
		admin.setEmail("xpto@ig.com");
		admin.setPassword(passwordEncoder.encode("123"));
		admin.setProfile(ProfileEnum.ROLE_ADMIN);
		
		User find = userRepository.findByEmail("xpto@ig.com");
		if(find == null) {
			userRepository.save(admin);
		}
	}
	
	
}
